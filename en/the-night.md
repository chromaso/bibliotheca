# The Night

- Author: J. A. Loftin
- Originally posted on: http://www.eunuchworld.org/view.php?storyid=1090

For some reason I have difficulty remembering when "The Night" actually happened. Was it weeks, months or years ago? I cannot say for sure. But the details of the experience are emblazoned upon my memory. Whenever I hear the music that was playing, catch a whiff of Obsession, or simply think of Manuella or Club Noir, I am practically paralyzed by instant crystal-clear, full-sensory recall. My recollection of the series of events that led me to be there on that fateful evening is also very clear, although not so profoundly vivid.

I am a hardcore bottom, and have been one basically since I became sexually active, just after my fourteenth birthday. I am also a hardcore lesbian - I have never even kissed a male, nor ever wanted to. I guess I first suspected I was not normal when I found myself attracted to Sally Walinski, a sixteen-year-old tomboy who lived next door to me. Losing my virginity by licking her pussy while she spanked me with a ping pong paddle confirmed my sexual leanings and set the tone for everything that followed.

As soon as I became aware that a "scene" existed, I began seeking out places and events where simpatico women congregated. At first it was hard to meet tops, particularly ones that were experienced at meting out the extreme punishments I craved. But once I had secured a fake i.d., I started hitting the leather bars with a vengeance and found no shortage of opportunities to bottom with long-time practitioners.

I found that I possessed a seemingly boundless appetite for submission and pain. Although I never belonged to a mistress, I indulged in various body modifications that would have been expected of a personal slave - I always kept my lower body completely hairless; my nipples and clitoris were pierced; and I had a large tattoo on my ass that read: Pain Slut.

I never, ever used a safe word. Right from the beginning I had an enormous tolerance and desire for pain. Fisting, tit and clit torture, extreme enemas and severe whippings did not come close to reaching my limits. Unintentionally, I developed a reputation as a pain slut extraordinaire. In fact, many dommes declined to top me because they found it humiliating not to be able to make me beg for mercy.

Of course, other dommes looked upon the prospect of breaking me as a challenge. One such was Manuella, a beautiful raven-haired dyke who had recently relocated from Europe, and who considered herself to be the most sadistic mistress on the Continent. As soon as she learned of my dubious claim to fame, she resolved to seek me out and use me to demonstrate her prowess as a mistress..

On "The Night" I was sitting at my usual table at the club, nursing a drink and hoping for a little action. Manuella came over from the bar and sat down across from me. For many long moments she just looked at me intently with those penetrating black eyes of hers. I remember thinking how hot she looked, and getting all goose bumpy, as I waited for her to speak.

"I am the ultimate harsh mistress you've been searching for," she said finally, in a deep, clear voice.

Even though I had heard similar proclamations from other dommes, her words sent a thrill through my body and made me hope that she could live up to her boast.

"I would that it were so," I replied in a near-whisper.

"Oh, it is so, all right! Have a session with me tonight and I'll prove it to you. If I can't get you to cry "mercy," then I will become your slave if you want me to."

Now that got my attention! I wasn't sure just what I would do with a slave if I had one, but the prospect was intriguing. More importantly, though, for her to offer that lent credence to her claim - no domme would ever take such a risk if she thought there was even the remotest chance she could lose.

Nearly a minute passed as I considered my response.

"Nothing ventured, nothing gained," I said at last, nodding my agreement to her proposal,

With that she came around to my side of the table and kissed me long and hard, snaking her tongue down my throat and sucking the breath from my lungs. Then she grabbed both of my nipples, lifted me to my feet and led me toward the stage.

When we reached the platform in the center of the club, a spotlight suddenly came on; apparently Manuella had informed the owners of her intentions. She introduced us to the audience and explained to them the proposition she had made - several gasps were heard, then a hush fell over the room.

Silently, matter-of-factly, she began to undress me. Occasionally she paused to gaze upon my lithe body or comment on some feature of my anatomy. When I was completely naked, I knelt - knees spread wide - in the appropriate place and allowed straps protruding from the floor to be fastened just behind my knees and around my ankles. Once this was accomplished, a wooden stock was secured around my neck and wrists; this was connected to a winch and pulled upward as far as possible. With the exception of the front lower portion of my legs, every part of my body was vulnerable.

Manuella left me on display for awhile for the audience's enjoyment ... and to heighten the anticipation for both of us. While I waited for the session to start in earnest, haunting cello music started emanating from the PA system. When she came back into my line of vision, I could see that she, too, was naked. I noticed also that she followed the European custom of not removing leg and underarm hair. For some reason this excited me more than the fact that she was nude.

Without a word she approached me and thrust her hirsute mons in my face. I nuzzled her sex and took a deep breath; I was rewarded with an intoxicating scent that was an exquisite mix of perfume and sexual arousal. Then, as expected, I used my tongue and lips to thank Manuella in advance for the experience that was to come.

After I had induced several orgasms, she pulled away and retrieved a satchel that contained the items she brought for the occasion. The first thing she removed from the bag was a mouthpiece, the type that boxers use. She shoved this in my mouth and, while I mused about the need for it, cut a strip of duct tape and smoothed it over my lips, effectively muzzling me.

The next item she produced made me start to think I might be in trouble, that this mistress was indeed more skilled than all the others I had encountered. What she held in her hand was an electric barber's razor. I began to cry silently as she came to me and ran her fingers through my shoulder-length auburn hair, looking down at me and smiling wickedly. She proceeded to shave my head, and as each lock of hair fell I became increasing aroused by the feeling of total submission that overcame me.

After all my hair was gone, this intense sensation was accentuated when she placed a blindfold over my eyes, plunging me into total darkness. I felt her fingers stroking my neck and temples, then her warm breath on my ear.

"That's all the foreplay for tonight," she whispered. "I've got a brand new cane I want to break in, so I'm just going to forego the preliminaries and get right to it."

Her words caused me to quiver a bit. Anyone familiar with corporeal punishment is aware how much pain a cane can inflict. Even for an aficionado like me, generally the cane is reserved until after the endorphins have kicked in from a workout with a flogger and/or crop. I certainly wasn't expecting her to start on the balls of my beet, which she did without warning or restraint. I was glad to have the mouthpiece to bite down on - the pain was excruciating.

With systematic precision, she traversed my backside from bottom to top with her cruel instrument, leaving burning strips of flesh in her wake. Despite the tremendous arousal this evoked in me, the pain was too much too fast for me to find the refuge of subspace, and thus I experienced the raw savagery being done to my body at full intensity.

Not missing a beat, Manuella came around to my front and continued her assault, again starting at the bottom and working her way upward. The onslaught was relentless and the pain way more extreme than anything I had ever endured. Had I been able, very possibly I would have used my safe word (which, foolishly, I had dismissed as unnecessary) as my breasts were being brutalized and probably permanently scarred. I knew then that Manuella was indeed all that she claimed to be. I began to worry just how far beyond my breaking point she would take me.

Suddenly the beating stopped, but not the pain as my body was a mass of angry welts and wounds. Labored breathing, both Manuella's and mine, was all that could be heard above the soulful lower-register strains of the cello. The pungent aromas of sweat and sex hung like a mist over the stage.

The blindfold was removed without warning, and when my eyes adjusted to the light I saw Manuella positioning a full-length mirror several feet in front of me.

"I will be making some body modifications and I want you to watch," she said ominously.

As she spoke, I noticed a familiar-looking tool in her hand. The main part consisted of a small wheel with spikes of varying sizes protruding outward; at the base was a squeezing mechanism that allowed a circular fitting to be tightened over whichever of the spikes it was aligned with. I finally recognized it as a leather punch, and my stomach tensed as I speculated on how it was going to be used.

I didn't have long to fret; Manuella immediately grabbed one of my outer labia, placed the device where she wanted it, and punched the first of eight holes. She then proceeded to do the same on the other one. The tool was extremely sharp, so there was surprisingly little pain. The feeling of total violation and helplessness, however, was almost overwhelming. Sick puppy that I am, this caused an orgasm that nearly made me swoon.

Before I had a chance to recover, Manuella started placing hog rings through the corresponding holes on each labium and crimping the rings closed with specialized pliers. When she was done, my slit was completely sealed except for a small opening at the base, which was left presumably to allow for bodily functions. I was transfixed by the vision of the bald woman with welts all over her body and rings securing her sex reflected in the mirror before me.

The spell was broken when Manuella knelt before me and began suckling first one nipple and then the other. After pleasuring me thus for perhaps five minutes, she stood and again went to her bag in search of something.

"I hope you enjoyed that, because you won't ever have that opportunity again," she said when she returned.

I wondered what she meant by that ... until I saw the fillet knife in her hand. For the first time in my life I felt real terror. This was more than I had bargained for.

"Since this is such a special occasion, I thought it would be nice to have some mementos to remember it by."

With that, she took hold of the ring in my left nipple and pulled it to her, extending my painfully erect teat away from my body. She then carefully sliced through the turgid flesh along the contour of the mound, the same as if she were trimming a morsel of fat from a select piece of meat. She held her souvenir between her teeth as she repeated the process on my right nipple. The pain was relatively negligible, but the sense of loss was truly profound. Manuella dabbed the wounds with a solution that staunched the bleeding, and then stepped back to admire her handiwork, smiling evilly as she did.

My worst fears were confirmed when that wonderfully cruel goddess knelt before me again and placed her tongue upon my clitoris. My entire body trembled uncontrollably as alternate waves of fear and arousal washed over me - my doomed little nub was rock hard ... and on fire.

"The memory of this will have to last you a lifetime, so let yourself go with it ... I'll give you your best - and last - orgasm."

And she did just that. Her velvety tongue teased and pleasured me for what seemed an eternity. She took me to the edge, and then back, again ... and again ... and again. Finally, when we both knew I was ready, she took me over the top with a mind-numbing, shuddering climax that was, in fact, the most amazing orgasm of my short life. While my entire nervous system still tingled with electricity, Manuella permanently ended the possibility of any future clitoral pleasure with a deft flick of her knife, and then swabbed the cut with the same solution she used on my breasts. At this point, I was prepared to beg for mercy, but I wasn't given the chance.

Sometime during the previous interlude, a brazier was brought on stage and placed off to the side. Manuella went to it and returned with a red-hot branding iron in the shape of an M, which she showed me briefly before going behind me and pressing it against my ass checks, one after the other, long enough to leave deep scars for posterity. The pain was indescribable. I blacked out for a few seconds, and when I awoke the searing pain had barely diminished.

Before I had even begun to recover, she went to the brazier again and returned with another red-hot iron; this one spelled a single word—EUNUCH. She shoved the iron into the soft flesh of my abdomen, about two inches above where my clitoris used to be. Had it not been for the mouthpiece, I surely would have bitten off my tongue, so horrible was the pain. I was drenched in sweat, and rivulets of tears streamed down my cheeks. Again I blacked out momentarily. When I returned to consciousness, Manuella was standing in front of me with the first branding iron, poised for action.

"I'm just getting started, I hope you're not ready to quit yet," she said. "I rather fancy having my brand on each of your cheeks. Are you up for that, my little eunuch?"

As much as I was able within the confines of the stock, I shook my head to indicate that I definitely wasn't up for that, and tried my best to communicate my surrender with my eyes. She brought the iron within an inch of my face - the heat nearly singed my eyebrow. I was so scared that I started to pee, and soon I was kneeling in a puddle of my own urine. Normally I would have been totally humiliated to have done such a thing, but I was so consumed with terror that I didn't give it a thought. Manuella looked down at the mess I had made and snorted.

"Could it be our pain slut has met her match? Are you ready to say the magic word to end this?"

I shook my head affirmatively as hard as I could. With her free hand she tore the tape from my mouth and extracted the mouthpiece.

"Well, go ahead. And make sure you say it nice and loud so they can hear you in the back!"

"Mercy, mistress! Please stop!" I practically yelled it, lest I displease her.

"Is that all?"

"You are the cruelest of all mistresses, bar none!"

"And ... ?"

"Please allow me to be your slave and serve you in whatever manner you wish."

"Yes, I think that can be arranged, especially since you've now been properly broken in. You can start by showing your appreciation for your transformation."

Her sex was drenched and her clitoris was so engorged it resembled a small penis. The first orgasm was almost instantaneous upon my tongue making contact, and the others came fast and furious - how many I cannot recall - over the remainder of the evening until Manuella was finally spent. After she dressed, she locked a stainless steel collar around my neck and led me by a leash to her car, leaving my clothes ... and my former life behind.

"The Night" was such a transformational event that I no longer have the ever-present hunger for pain and submission that used to be central to my life. I am satiated now.