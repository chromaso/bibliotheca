# 目录

* [欢迎](README.md)

---

* [原创作品](original.md)
    * [学姐的鞋底，学弟的天堂](https://chromaso.net/story/18/)
    * [偏偏要做你的 M](https://chromaso.net/story/19/)

---

## 中文转载（男 M，刑虐向）

* [逝去的灵魂](zh/shi-qu-de-ling-hun.md)
* [林静瑶/被囚禁在地狱般的高中](zh/lin-jing-yao.md)
* [生死虐杀](zh/sheng-si-nue-sha.md)
* [SM娇妻（曾经是兽医的三陪女郎）](zh/sm-jiao-qi.md)
* [恶毒的妹妹](zh/e-du-de-mei-mei.md)
* [血色之虐恋情缘（女王阉奴）](zh/xue-se-zhi-nue-lian-qing-yuan.md)
* [辛迪的一天](zh/xin-di-de-yi-tian.md)
* [阉了老公](zh/yan-le-lao-gong.md)

## 中文转载（男 M，绿帽向）

* [调教老公成绿奴](zh/tiao-jiao-lao-gong-cheng-lyu-nu.md)
* [爱上一个有男朋友的女孩](zh/ai-shang-yi-ge-you-nan-peng-you-de-nyu-hai.md)
* [他的胯下是我最爱的女友](zh/ta-de-kua-xia-shi-wo-zui-ai-de-nyu-you.md)
* [喜欢带绿帽的老公（我的妻子阉了我）](zh/xi-huan-dai-lyu-mao-de-lao-gong.md)
* [自我阉割的绿帽王八](zh/zi-wo-yan-ge-de-lyu-mao-wang-ba.md)
* [妻子满足我的绿帽需求（我最爱的老婆）](zh/qi-zi-man-zu-wo-de-lyu-mao-xu-qiu.md)
* [给前夫的信（撒旦的眼泪）](zh/gei-qian-fu-de-xin.md)
* [矛盾的欲望——一个绿帽Ｍ的心路历程](zh/mao-dun-de-yu-wang.md)
* [网吧女厕](zh/wang-ba-nyu-ce.md)

## 中文转载（男 M，纯恋足）

* [棉袜臭脚下的哥哥](zh/mian-wa-chou-jiao-xia-de-ge-ge.md)
* [高中女子足球队的脚垫](zh/gao-zhong-nyu-zi-zu-qiu-dui-de-jiao-dian.md)

## 中文转载（女 M）

* [我和小柔](zh/wo-he-xiao-rou.md)
* [处决的伴侣](zh/execution-partners.md)
* [依云的献身](zh/yi-yun-de-xian-sheng.md)
* [虐殺艾米麗](zh/nue-sha-ai-mi-li.md)
* [淫缚艳杀](zh/yin-fu-yan-sha.md)
* [恶毒，彻底销毁！](zh/e-du-che-di-xiao-hui.md)
* [人间故事（2）](zh/ren-jian-gu-shi-2.md)
* [错过的旧旋律](zh/cuo-guo-de-jiu-xuan-lyu.md)
* [离歌](zh/li-ge.md)

## English stories

* [A Tip for Mistress \[F/m\]](en/a-tip-for-mistress.md)
* [An Encounter with Goddess Demoness \[F/m\]](en/an-encounter-with-goddess-demoness.md)
* [December 1820 \[F/f\]](en/december-1820.md) 
* [Encounter With Mistress Tanya \[F/m\]](en/encounter-with-mistress-tanya.md)
* [In Due Time \[F/m\]](en/in-due-time.md)
* [Judy's Castration Jewels \[F/m\]](en/judys-castration-jewels.md)
* [Mistress Queens - Teased, Tortured, CASTRATED \[F/m\]](en/mistress-queens-teased-tortured-castrated.md)
* [More Convincing Than I Thought \[F/m\]](en/more-convincing-than-i-thought.md)
* [The Night \[F/f\]](en/the-night.md)
* [The Last Night Of Nikki \[M/f\]](en/the-last-night-of-nikki.md)
